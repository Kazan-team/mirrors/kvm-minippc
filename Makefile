CFLAGS += -Wall -Wextra -g
LDFLAGS += -Wl,-O1

NAME = kvm-minippc
SRC = $(wildcard *.c)

.PHONY: all clean

all: $(NAME)

$(NAME): $(SRC)
	$(CC) $(SRC) -o $(NAME) $(CFLAGS) $(LDFLAGS)

clean:
	rm -f *.o $(NAME)
